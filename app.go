package main

import (
	"fmt"
)

func (app *App) makeNewCaptcha() (string, error) {
	captchaId, err := generateRandomString(20)

	if err != nil {
		return "", err
	}

	var word Word
	app.db.Order("RANDOM()").Take(&word)

	captcha := Captcha{ID: captchaId, Word: word}
	app.db.Create(&captcha)

	return captcha.ID, nil
}

func (app *App) getCaptchaImage(captchaId string) ([]byte, error) {
	captcha, err := app.loadCaptcha(captchaId)
	if err != nil {
		return nil, err
	}

	imageBytes, err := app.imageGenerator.GenerateCaptchaImage(captchaId, captcha.Word.Text)
	if err != nil {
		return nil, err
	}

	return imageBytes, nil
}

func (app *App) loadCaptcha(captchaId string) (Captcha, error) {
	var captcha Captcha
	app.db.Preload("Word").First(&captcha, "id = ?", captchaId)

	if captcha.ID == "" {
		return Captcha{}, fmt.Errorf("Captcha not found")
	}

	return captcha, nil
}

func (app *App) verifyCaptcha(captchaId string, input string) bool {
	captcha, err := app.loadCaptcha(captchaId)
	if err != nil {
		return false
	}

	return captcha.Word.Text == input
}
