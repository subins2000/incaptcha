(function () {
  function rand(min, max) {
    // min and max included
    return Math.floor(Math.random() * (max - min + 1) + min);
  }

  const fillWithCaptcha = (element, word) => {
    element.style.fontFamily = "Nupuram";

    const segmenterMl = new Intl.Segmenter("ml", { granularity: "grapheme" });
    const wordGraphemes = [...segmenterMl.segment(word)].map((x) => x.segment);

    for (let char of wordGraphemes) {
      const span = document.createElement("span");
      span.style.display = "inline-block";
      // span.style.transform = `rotate(${rand(-25, 25)}deg)`;
      span.style.fontVariationSettings = `"wght" ${rand(
        100,
        900
      )}, "wdth" ${rand(75, 125)}, "soft" ${rand(0, 100)}, "slnt" -${(0, 15)}`;
      span.innerText = char;
      element.appendChild(span);
    }
  };

  const resultsContainer = document.getElementById("results");

  const generateCaptchaImage = (captchaId, word) => {
    const div = document.createElement("div");
    div.setAttribute("id", captchaId);

    div.style.display = "inline-block";
    div.style.padding = "0 5px 10px 5px";
    div.style.letterSpacing = "5px";
    div.style.fontSize = "3rem";

    resultsContainer.appendChild(div);
    fillWithCaptcha(div, word);
  };

  window.generateCaptchaImage = generateCaptchaImage;
})();
