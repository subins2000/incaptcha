package image_generator

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/chromedp/chromedp"
	"github.com/knadh/stuffbin"
	"github.com/labstack/echo/v4"
)

type GeneratorApp struct {
	chromeContext context.Context
	vfs           stuffbin.FileSystem
}

var imageGeneratorServicePort = "8201"
var imageGeneratorServiceUrl = "http://localhost:" + imageGeneratorServicePort

func InitImageGenerator() (*GeneratorApp, context.CancelFunc) {
	vfs, err := initVFS()
	if err != nil {
		log.Fatal(err.Error())
	}

	ctx, cancel := chromedp.NewContext(
		context.Background(),
		// chromedp.WithDebugf(log.Printf),
	)

	generatorApp := &GeneratorApp{ctx, vfs}

	go generatorApp.initGeneratorWebServer()

	chromedp.Run(generatorApp.chromeContext, chromedp.Tasks{
		chromedp.Navigate(imageGeneratorServiceUrl),
	})

	return generatorApp, cancel
}

func initVFS() (stuffbin.FileSystem, error) {
	files := []string{
		"image_generator/web:/",
	}

	binPath, err := os.Executable()
	if err != nil {
		return nil, err
	}

	vfs, err := stuffbin.UnStuff(binPath)
	if err != nil {
		log.Printf("unable to initialize embedded filesystem: %v", err)
		log.Printf("using local filesystem")

		vfs, err = stuffbin.NewLocalFS("/", files...)
		if err != nil {
			log.Fatalln(err.Error())
			return nil, err
		}
	}

	return vfs, nil
}

func (generatorApp *GeneratorApp) initGeneratorWebServer() error {
	e := echo.New()

	// Custom middleware to set sigleton to app's context.
	e.Use(func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			c.Set("generatorApp", generatorApp)
			return next(c)
		}
	})

	e.GET("/", handleIndex)
	e.GET("/*", echo.WrapHandler(generatorApp.vfs.FileServer()))
	e.Logger.Fatal(e.Start(":" + imageGeneratorServicePort))

	return nil
}

func (generatorApp *GeneratorApp) GenerateCaptchaImage(captchaId string, word string) ([]byte, error) {
	var res []string
	var imageBytes []byte

	err := chromedp.Run(generatorApp.chromeContext, chromedp.Tasks{
		chromedp.Evaluate(fmt.Sprintf("generateCaptchaImage('%v', '%v')", captchaId, word), &res),
		chromedp.Screenshot(fmt.Sprintf("div#%v", captchaId), &imageBytes, chromedp.NodeVisible),
	})

	log.Println(res)

	if err != nil {
		log.Println(err)
	}

	return imageBytes, nil
}

// handleIndex is the root handler that renders the Javascript frontend.
func handleIndex(context echo.Context) error {
	generatorApp, _ := context.Get("generatorApp").(*GeneratorApp)

	b, err := generatorApp.vfs.Read("/index.html")
	if err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
	}

	context.Response().Header().Set("Content-Type", "text/html")

	return context.String(http.StatusOK, string(b))
}
