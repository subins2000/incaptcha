module gitlab.com/subins2000/incaptcha

go 1.16

require (
	github.com/chromedp/cdproto v0.0.0-20230625224106-7fafe342e117 // indirect
	github.com/chromedp/chromedp v0.9.1 // indirect
	github.com/gobwas/ws v1.2.1 // indirect
	github.com/jinzhu/now v1.1.5 // indirect
	github.com/knadh/stuffbin v1.1.0
	github.com/labstack/echo/v4 v4.6.1
	github.com/mattn/go-sqlite3 v1.14.15 // indirect
	golang.org/x/sys v0.9.0 // indirect
	gorm.io/driver/sqlite v1.3.6
	gorm.io/gorm v1.23.8
)
