package main

import (
	"fmt"
	"net/http"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

func startServer(app *App) {
	e := echo.New()

	e.POST("/new_captcha", handleNewCaptcha)
	e.GET("/captcha/:id", handleCaptchaImage)
	e.POST("/captcha/:id/attempt", handleAttemptCaptcha)

	e.GET("/", handleIndex)
	e.GET("/*", echo.WrapHandler(app.fs.FileServer()))

	// Custom middleware to set sigleton to app's context.
	e.Use(func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			c.Set("app", app)
			return next(c)
		}
	})

	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowMethods: []string{http.MethodOptions},
	}))

	e.Logger.Fatal(e.Start(":8200"))
}

func respondBadRequest(err error) error {
	return echo.NewHTTPError(http.StatusBadRequest, fmt.Sprintf("%s", err.Error()))
}

func handleNewCaptcha(context echo.Context) error {
	app := context.Get("app").(*App)

	id, err := app.makeNewCaptcha()
	if err != nil {
		return respondBadRequest(err)
	}

	return context.String(http.StatusOK, id)
}

func handleCaptchaImage(context echo.Context) error {
	app := context.Get("app").(*App)
	captchaId := context.Param("id")

	imageBytes, err := app.getCaptchaImage(captchaId)

	if err != nil {
		return respondBadRequest(err)
	}

	return context.Blob(http.StatusOK, "image/png", imageBytes)
}

func handleAttemptCaptcha(context echo.Context) error {
	app := context.Get("app").(*App)
	captchaId := context.Param("id")

	input := context.FormValue("input")

	if app.verifyCaptcha(captchaId, input) {
		return context.String(http.StatusOK, "correct")
	}

	return context.String(http.StatusOK, "incorrect")
}

// handleIndex is the root handler that renders the Javascript frontend.
func handleIndex(context echo.Context) error {
	app, _ := context.Get("app").(*App)

	b, err := app.fs.Read("/index.html")
	if err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
	}

	context.Response().Header().Set("Content-Type", "text/html")

	return context.String(http.StatusOK, string(b))
}
