package main

import (
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

// Word word
type Word struct {
	gorm.Model
	ID   uint64 `gorm:"primaryKey"`
	Lang string
	Text string `gorm:"uniqueIndex"`
}

// Captcha stores a captcha session
type Captcha struct {
	gorm.Model
	ID     string `gorm:"primaryKey,uniqueIndex"`
	WordID int
	Word   Word
}

func connectToDB() *gorm.DB {
	db, err := gorm.Open(sqlite.Open("db.sqlite3"), &gorm.Config{})
	if err != nil {
		panic("failed to connect database")
	}

	// Migrate the schema
	db.AutoMigrate(&Word{}, &Captcha{})

	return db
}
