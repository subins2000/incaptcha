import { defineConfig } from "vite";
import * as path from "path";

import vue from "@vitejs/plugin-vue";

// https://vitejs.dev/config/
export default defineConfig({
  build: {
    emptyOutDir: true,

    lib: {
      entry: path.resolve(__dirname, "./src/main.ts"),
      formats: ["iife"],
      name: "Incaptcha",
      fileName: () => `embed.js`,
    },
    target: "modules",
    minify: "terser",
  },
  plugins: [vue({reactivityTransform: true})]
});
