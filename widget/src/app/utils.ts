const API_URL = location.hostname === "localhost" ? "http://localhost:8200" : "/";

export const generateCaptcha = async () => {
  const response = await fetch(`${API_URL}/new_captcha`, { method: "POST" });
  if (response.status === 200) {
    const captchaId = await response.text();
    return {
      captchaId,
      imageUrl: `${API_URL}/captcha/${captchaId}`,
    };
  }
};

export const attemptCaptcha = async (captchaId, input) => {
  const formData = new URLSearchParams()
  formData.append("input", input);

  const response = await fetch(`${API_URL}/captcha/${captchaId}/attempt`, {
    headers: {
      "Content-Type": "application/x-www-form-urlencoded",
    },
    method: "POST",
    body: formData.toString(),
  });
  const body = await response.text();
  return body === "correct";
};
