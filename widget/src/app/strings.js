export default {
  en: {
    enterCaptcha: "Please enter the above word",
    submit: "Submit",
  },
  ml: {
    enterCaptcha: "മുകളിൽ തന്നിരിക്കുന്ന വാക്ക് ഇവിടെ ടൈപ്പ് ചെയ്യുക",
    submit: "സമർപ്പിക്കുക",
  },
};
