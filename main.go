package main

import (
	"log"
	"os"

	"github.com/knadh/stuffbin"
	"gitlab.com/subins2000/incaptcha/image_generator"
	"gorm.io/gorm"
)

// App is a singleton to share across handlers.
type App struct {
	db             *gorm.DB
	fs             stuffbin.FileSystem
	imageGenerator *image_generator.GeneratorApp
}

func main() {
	db := connectToDB()
	fs, err := initVFS()
	if err != nil {
		log.Fatal(err.Error())
	}

	imageGenerator, cancel := image_generator.InitImageGenerator()
	defer cancel()

	app := &App{db, fs, imageGenerator}
	startServer(app)
}

// initVFS initializes the stuffbin virtual FileSystem to provide
// access to bunded static assets to the app.
func initVFS() (stuffbin.FileSystem, error) {
	files := []string{
		"widget/dist:/",
		"web:/",
	}

	binPath, err := os.Executable()
	if err != nil {
		return nil, err
	}

	fs, err := stuffbin.UnStuff(binPath)
	if err != nil {
		log.Printf("unable to initialize embedded filesystem: %v", err)
		log.Printf("using local filesystem")

		fs, err = stuffbin.NewLocalFS("/", files...)
		if err != nil {
			return nil, err
		}
	}

	return fs, nil
}
